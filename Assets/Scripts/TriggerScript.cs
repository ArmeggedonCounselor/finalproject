﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScript : MonoBehaviour {
	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "Crate") {
			GameManager.manager.HandleTriggers (gameObject, true);
			GameManager.manager.am.PlayClip ("Trigger Activated");
		}
	}

	void OnTriggerExit2D(Collider2D other){
		if (other.gameObject.tag == "Crate") {
			GameManager.manager.HandleTriggers (gameObject, false);
		}
	}
}
