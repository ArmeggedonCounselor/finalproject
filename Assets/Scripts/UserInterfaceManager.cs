﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserInterfaceManager : MonoBehaviour {

	public Canvas screen;
	public Sprite lostLife;
	public Sprite lifeCounter;

	public void LifeCounters (int livesLeft){
		if (screen == null) {
			screen = FindObjectOfType<Canvas> ();
		}
		Image[] liveCounters = screen.GetComponentsInChildren<Image> ();
		for (int i = 0; i < livesLeft; i++) {
			liveCounters [i].overrideSprite = lifeCounter;
		}
	}

	public void PlayGame(){
		GameManager.manager.lives = 3;
		GameManager.manager.currentLevel = 1;
		GameManager.manager.scene.Load ("Level1");
	}

	public void Options(){
		GameManager.manager.scene.Load ("Options");
	}

	public void Menu() {
		GameManager.manager.scene.Load("Menu");
	}

	void Lose() {
		GameManager.manager.scene.Load ("Lose");
	}

}
