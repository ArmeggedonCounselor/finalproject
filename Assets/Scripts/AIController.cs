﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : Controller {

	// Use this for initialization
	public override void Start () {
		// Run the parents Start
		base.Start();
	}
	
	// Update is called once per frame
	public override void Update () {
		// Run the parents Update
		base.Update();
	}
}
