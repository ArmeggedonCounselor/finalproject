﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public static GameManager manager;
	[Header("Managers")]
	public SceneLoader scene;
	public UserInterfaceManager UI;
	public AudioManager am;

	[Header("Objects")]
	public List<Controller> entities;

	public List<Goal> goals;
	[Header("Numbers")]
	public int numberLevels;
	public int currentLevel;
	public int levelTiming = 30;
	public int lives;

	private bool done = false;

	private int levelTimer;

	public struct Goal{
		public GameObject goal;
		public bool crateOnGoal;
	};

	void Awake(){
		if (manager != null) {
			//Debug.Log ("There can only be one GameManager. Count: " + count);
			Destroy (gameObject);
		}
		manager = this;
		DontDestroyOnLoad (gameObject);
	}

	// Use this for initialization
	void Start () {
		entities = new List<Controller> ();
		goals = new List<Goal> ();
		currentLevel = 0;
		lives = 3;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			lives--;
			scene.Load (scene.loadedScene());
			UI.LifeCounters (lives);
		}
		if (lives == -1) {
			lives = 0;
			Lose ();
		}
		if (done) {
			levelTimer++;
			if (levelTimer == levelTiming) {
				levelTimer = 0;
				NextLevel ();
			}
		}

		//if (GameObject.FindGameObjectWithTag ("GameManager") != gameObject) {
		//	Destroy (GameObject.FindGameObjectWithTag ("GameManager"));
		//}
		if (UI.screen == null) {
			UI.screen = GameObject.FindObjectOfType<Canvas> ();
		}
	}

	public void HandleTriggers(GameObject caller, bool enter) {
		Goal temp = new Goal ();
		temp.goal = caller;
		temp.crateOnGoal = enter;
		for (int i = 0; i < goals.Count; i++) {
			if (goals [i].goal == caller) {
				goals.RemoveAt (i);
				goals.Add (temp);
			}
		}
		if (AllGoalsMet ()) {
			levelTimer = 0;
			done = true;
		}
	}

	public void MakeGoals(List<GameObject> objects){
		Goal temp = new Goal ();
		foreach (GameObject item in objects) {
			temp.goal = item;
			temp.crateOnGoal = false;
			goals.Add (temp);
		}
	}

	bool AllGoalsMet(){
		bool allMet = true;
		foreach (Goal goal in goals) {
			if (!allMet) {
				return false;
			}
			allMet = goal.crateOnGoal;
		}
		return true;
	}

	void NextLevel() {
		if (currentLevel == numberLevels) {
			currentLevel = 0;
			done = false;
			Win ();
		} else {
			goals.Clear ();
			done = false;
			currentLevel++;
			scene.Load (scene.sceneIndex () + 1);
		}
	}

	void Win(){
		scene.Load ("Win");
	}

	void Lose() {
		scene.Load ("Lose");
	}

	public void Exit(){
		Application.Quit ();
	}
}
