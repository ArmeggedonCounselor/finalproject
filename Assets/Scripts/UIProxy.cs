﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIProxy : MonoBehaviour {
	private string scene;

	void Start(){
		scene = GameManager.manager.scene.loadedScene ();
		if (scene != "Menu" && scene != "Options" && scene != "Win" && scene != "Lose") {
			GameManager.manager.UI.LifeCounters (GameManager.manager.lives);
		}
	}

	public void Menu(){
		GameManager.manager.UI.Menu ();
	}

	public void PlayGame() {
		GameManager.manager.UI.PlayGame ();
	}

	public void Options() {
		GameManager.manager.UI.Options ();
	}

	public void Exit() {
		GameManager.manager.Exit ();
	}
}
