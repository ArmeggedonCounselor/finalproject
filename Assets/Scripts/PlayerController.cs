﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller {
	public int holdRepeat = 30;
	public float stepDistance = 1f;

	private int holdTimer;
	private SpriteChanger sc;
	private Transform tf;

	// Use this for initialization
	public override void Start () {
		// Run the Start from parent class
		base.Start ();
		tf = GetComponent<Transform> ();
		sc = GetComponent<SpriteChanger> ();
		holdTimer = holdRepeat;
	}
	
	// Update is called once per frame
	public override void Update () {
		base.Update ();
	}

	void FixedUpdate(){
		if (Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.UpArrow)) {
			sc.ChangeSprite (KeyCode.W);
			if (holdTimer == holdRepeat) {
				holdTimer = 0;
				if (CheckIfClear (0)) {
					tf.position += Vector3.up * stepDistance;
				}
			}
		}
		if (Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.LeftArrow)) {
			sc.ChangeSprite (KeyCode.A);
			if (holdTimer == holdRepeat) {
				holdTimer = 0;
				if (CheckIfClear (3)) {
					tf.position += Vector3.left * stepDistance;
				}
			}
		}
		if (Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.RightArrow)) {
			sc.ChangeSprite (KeyCode.D);
			if (holdTimer == holdRepeat) {
				holdTimer = 0;
				if (CheckIfClear (1)) {
					tf.position += Vector3.right * stepDistance;
				}
			}
		}
		if (Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.DownArrow)) {
			sc.ChangeSprite (KeyCode.S);
			if (holdTimer == holdRepeat) {
				holdTimer = 0;
				if (CheckIfClear (2)) {
					tf.position += Vector3.down * stepDistance;
				}
			}
		}
		if (!Input.anyKey) {
			holdTimer = holdRepeat;
		}
		if (Input.anyKey && holdTimer < holdRepeat) {
			holdTimer++;
		}
	}

	// Direction is given as an integer from 0 - 3. 0 is up, 1 is right, 2 is down, and 3 is left.
	 bool CheckIfClear(int direction){
		Vector2 origin = tf.position;
		Vector2 dir = Vector2.zero;
		RaycastHit2D inFront = new RaycastHit2D();
		RaycastHit2D[] hit;
		switch (direction) {
		case 0:
			dir = Vector2.up;
			break;
		case 1:
			dir = Vector2.right;
			break;
		case 2:
			dir = Vector2.down;
			break;
		case 3:
			dir = Vector2.left;
			break;
		}

		inFront = Physics2D.Raycast (origin, dir, stepDistance);
		hit = Physics2D.RaycastAll (origin, dir, (stepDistance * 2));
		if (inFront.collider != null) {
			if (inFront.collider.gameObject.tag == "Wall") {
				GameManager.manager.am.PlayClip ("Bump");
				return false;
			}
			if (inFront.collider.gameObject.tag == "Crate") {
				if (hit.Length > 1) {
					GameObject last = hit [hit.Length - 1].collider.gameObject;
					if (last.tag == "Wall" || last.tag == "Crate") {
						GameManager.manager.am.PlayClip ("Bump");
						return false;
					}
				}
				inFront.collider.gameObject.transform.position += (Vector3) dir * stepDistance;
				GameManager.manager.am.PlayClip ("Box Scrape");
				return true;
			}
		}
		GameManager.manager.am.PlayClip ("Step");
		return true;
		
	}
}
