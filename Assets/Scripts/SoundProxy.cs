﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundProxy : MonoBehaviour {
	public float bgmVolume { get; set; }
	public float sfxVolume { get; set; }

	void Start(){
		bgmVolume = 1f;
		sfxVolume = 1f;
	}

	void Update(){
		GameManager.manager.am.bgmVolume = bgmVolume;
		GameManager.manager.am.sfxVolume = sfxVolume;
	}

}
