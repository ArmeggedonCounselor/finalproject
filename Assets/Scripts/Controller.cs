﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {

	// Use this for initialization
	virtual public void Start () {
		// Add myself to game manager list
		if (GameManager.manager.entities != null) {
			GameManager.manager.entities.Add (this);		
		} else {
			Debug.LogError ("ERROR: NO GAME MANAGER OR ENTITY LIST.");
		}
		
	}
	
	// Update is called once per frame
	virtual public void Update () {
		
	}

	virtual public void OnDestroy(){
		if (GameManager.manager.entities != null) {
			GameManager.manager.entities.Remove (this);
		} else {
			Debug.LogError ("ERROR: NO GAME MANAGER OR ENTITY LIST.");
		}
	}
}
