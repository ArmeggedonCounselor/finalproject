﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {
	public float sfxVolume;
	public float bgmVolume;

	public AudioSource bgmSource;
	public AudioSource sfxSource;
	private bool isPlaying;

	[Header("Audio Clips")]
	public List<AudioClip> clips;

	void Start(){
		isPlaying = false;
		bgmVolume = 1f;
		sfxVolume = 1f;
	}

	void Update(){
		if (!isPlaying) {
			bgmSource.volume = 1f;
			bgmSource.loop = true;
			bgmSource.Play ();
			isPlaying = true;
		}
		if (isPlaying) {
			bgmSource.volume = bgmVolume;
			sfxSource.volume = sfxVolume;
		}
	}

	public void PlayClip(string clip){
		if (sfxSource == null) {
			sfxSource = GameObject.FindGameObjectWithTag ("SFX Slave").GetComponent<AudioSource> ();
		}
		sfxSource.volume = sfxVolume;
		foreach(AudioClip item in clips){
			if (item.name == clip) {
				sfxSource.clip = item;
				break;
			}
		}
		sfxSource.Play ();
	}
}
