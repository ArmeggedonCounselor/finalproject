﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

	public void Load(string scene){
		SceneManager.LoadScene (scene);
	}

	public void Load(int index){
		SceneManager.LoadScene (index);
	}

	public string loadedScene(){
		return SceneManager.GetActiveScene ().name;
	}

	public int sceneIndex(){
		return SceneManager.GetActiveScene ().buildIndex;
	}
		
}
