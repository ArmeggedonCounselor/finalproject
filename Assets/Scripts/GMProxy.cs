﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMProxy : MonoBehaviour {

	public List<GameObject> goals;
	private List<GameManager.Goal> test;

	// Use this for initialization
	void Start () {
		test = new List<GameManager.Goal> ();
		MakeGoals (goals);
		GameManager.manager.goals = test;
	}

	void MakeGoals(List<GameObject> objects){
		GameManager.Goal temp = new GameManager.Goal ();
		foreach (GameObject item in objects) {
			temp.goal = item;
			temp.crateOnGoal = false;
			test.Add (temp);
		}
	}
}
