﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteChanger : MonoBehaviour {

	private SpriteRenderer symbol;

	[Header("Sprites")]
	public Sprite forward;
	public Sprite back;
	public Sprite left;
	public Sprite right;

	// Use this for initialization
	void Start () {
		symbol = GetComponent<SpriteRenderer> ();
	}

	public void ChangeSprite (KeyCode key){
		switch (key) {
		case KeyCode.A:
		case KeyCode.LeftArrow:
			if (symbol.sprite != left) {
				symbol.sprite = left;
			}
			break;
		case KeyCode.W:
		case KeyCode.UpArrow:
			if (symbol.sprite != back) {
				symbol.sprite = back;
			}
			break;
		case KeyCode.S:
		case KeyCode.DownArrow:
			if (symbol.sprite != forward) {
				symbol.sprite = forward;
			}
			break;
		case KeyCode.D:
		case KeyCode.RightArrow:
			if (symbol.sprite != right) {
				symbol.sprite = right;
			}
			break;
		}
	}
}
